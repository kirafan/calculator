# 5. 解除异常 / 状態異常回復

![](../merge/Other-Confusion.png) ![](../merge/Other-Paralysis.png) ![](../merge/Other-Poison.png) ![](../merge/Other-Bearish.png) ![](../merge/Other-Sleep.png) ![](../merge/Other-Unhappy.png) ![](../merge/Other-Silence.png) ![](../merge/Other-Isolation.png) ![](../merge/Abnormal-Clear.png)

<WIP />

**`AbnormalRecover`。**解除目标的某种或全部异常。
