# 12. 下一次攻击暴击 / 次回の攻撃絶対クリティカル

![](../merge/Buff-NextLuck.png) 

<WIP />

**`NextAttackCritical`、クリティカル確定、必爆。**使下一次攻击一定触发暴击。

## 计算方式

在下一次攻击时跳过暴击率的计算，直接触发暴击，即使属性被克制。

## 持续时间

直到角色进行攻击后，本效果才会消失。即若角色不攻击，则下一次攻击提升一直会存在。

## 叠加方式

多个下一次攻击暴击之间互相覆盖。
